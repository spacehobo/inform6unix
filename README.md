**Welcome to Inform!**
=======================

---

Contains:

Inform6 Compiler 6.35
Inform6 Standard Library 6.12.4
PunyInform Library 2.5

Inform is an Interactive Fiction (text adventure) game compiler -- it takes
source code you write and turns it into a game data file which is then
played using an *interpreter*.  There are several interpreters available
which can play Inform games on different machines (e.g. frotz, jzip) -- you
can probably obtain one from the same place you got this package.

Inform was originally written by Graham Nelson, and you are free to
redistribute it under certain conditions -- see the file COPYING for
details.

This package contains the Inform6 compiler and two libraries.  First,
there is the Standard Library version 6.12.4.  The other is PunyInform
version 2.4.  PunyInform is intended for creating games with a much
smaller footprint than is possible with the Standard Library.  This
makes it much easier to create games that can be played on older
computers such as the Commodore 64, Apple II, and Atari 8-bit series to
name a few.

What's in this distribution?
----------------------------

The following subdirectories are included in the package:

- ***src***	    --- Source code for the Inform program
- ***std***	    --- Inform6 Standard Library
- ***std-include*** --- Selection of useful include files
- ***std-demos***   --- Some Inform6 demo games (including the classic *advent*)
- ***std-tutor***   --- Some Inform6 tutorial files
- ***punyinform***  --- Inform6 PunyInform Library with documentation 
- ***docs***	    --- Internal Inform documentation and release notes
- ***contrib***	    --- Other contributed Inform stuff

How do I install it?
--------------------

If you're working from a git repository, do this first:

    make submodules

Then, here's how to build Inform6:

    make
    sudo make install

This will install the following (assuming default installation):

    Inform executable (inform) in /usr/local/bin
    Inform Standard Library files in /usr/local/share/inform/std/
    Inform PunyInform Library files in /usr/local/share/inform/punyinform/

If you want to install Inform somewhere other than /usr/local, edit 
Makefile accordingly.

What if I don't get along with git?
-----------------------------------

You can't use the automatically-generated tarball downloaded from
Gitlab.  Download it from
http://ifarchive.org/indexes/if-archive/infocom/compilers/inform6/source/
instead.  Why?  Gitlab currently does not populate submodule
directories.  If you download a tarball from here, it will be missing
the contents of the src/ and lib/ directories.

OK, it's installed.  Now what?
------------------------------

There are three canonical works documenting the Inform6 language.  These 
are the Inform Designers Manual (4th ed), the Inform Beginner's Guide, 
and the IF Theory Reader.  These are at 
http://inform-fiction.org/manual/index.html.  At least the the Inform 
Designer's Manual is currently available on Amazon as a hardcopy 
hardcover book.  They're very nice to have on hand when going through 
the demos/ and tutor/ directories and follow along with the books. After 
that, you're all set to write an IF game!  Yay!

What's this PunyInform?
-----------------------

(From ifwiki.org)
PunyInform is meant to be a lightweight alternative to the Inform 6
library, specifically for authors writing games for 8-bit platforms.
Since games are compiled to Z-code, they can of course be played on more
modern platforms as well.  Compared to the Inform 6 Standard Library,
PunyInform is more compact and has been developed with a greater focus
on execution speed on slow platforms.  Also, it can compile to z3 format.

From a programmer's point of view, PunyInform is very similar to the
Inform 6 library.  PunyInform lacks a few of the standard library's
features, and some features are implemented in a different way to make
for smaller and faster code.  Some features are optional, so you can
decide if your project needs them or if you'd rather save some space.

A `punyinform.sh` script (symlinked with `punyinform`) is provided to
automatically set up the include path so that the Inform6 compiler will
use PunyInform's source files rather than those of the Standard Library.
Otherwise it behaves exactly like calling `inform` itself.

Sample usage: `punyinform -v5sD mygame.inf`

PunyInform's home is at https://github.com/johanberntsson/PunyInform.  
Official documentation for PunyInform can be found there and in
punyinform/documentation/ here.

Troubleshooting
---------------

If you have any problems with anything, contact the relevant person
listed in the AUTHORS file.  If you're not sure who that is, contact me
instead, at the address at the end of this file.

The Interactive Fiction archive
-------------------------------

There's a good chance that you got this package from the IF archive, or one
of its mirrors.  But if you didn't, you might like to check it out
http://www.ifarchive.org

It has lots of great things: games, hints, solutions, authoring systems
(like this one), programs for playing the games, tools for making maps, and
stuff about the late, great Infocom.

There are also more resources for programming with Inform, including a
version of the Inform Designer's Manual suitable for printing.  See the
stuff in the programming/inform6 subdirectory.

The Inform maintainers
----------------------

An active community of Inform maintainers exists to fix bugs, implement new
features and issue new versions of the program.  If you'd like to know
more, or you think you've found a bug, visit them at 
http://inform7.com/contribute/report/

About this package
------------------

This package was originally created by Glenn Hutchings to address the
tedium of gathering the program, libraries, and documentation for
several different Unix machines.  It received blessing from Graham
Nelson.  The result is a package that automates the configuration and
installation process.  It should build and install on all Unix, Linux,
and Win32/Cygwin system.

This package is an ideal base for creating pre-compiled packages in the
style of Debian .debs, Redhat .rpms and similar schemes as well as build
trees like FreeBSD ports, NetBSD pkgsrc, and Gentoo portage.

Many people contributed to the contents of this package.  See the file
AUTHORS for more details.

This package is hosted at Gitlab:

https://gitlab.com/DavidGriffith/inform6unix

Feel free to hack on it and send me improvements!

Finally...
----------

If you have any comments or suggestions (or anything else, for that matter)
feel free to drop me a line.  I am:

David Griffith <dave@661.org>.
