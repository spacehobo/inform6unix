# Makefile for Unix Inform 6 Compiler package
#
#   Inform 6.35
#   Inform 6 Standard Library 6.12.4
#   PunyInform Library 2.5
#
# GNU make is required.

# Your C compiler
#CC ?= gcc
#CC ?= clang

#OPTS = -g -Wall -Wextra
LDFLAGS = -lm # https://gitlab.com/DavidGriffith/inform6unix/-/issues/27

PREFIX ?= /usr/local
MAN_PREFIX = $(PREFIX)
COMPVERSION = 6.35
PKGVERSION = r1
NAME = inform
LANGUAGE = english
BINNAME = $(NAME)
DISTNAME = $(BINNAME)-$(COMPVERSION)-$(PKGVERSION)
UNAME := $(shell uname -s)

GIT = git
GIT_DIR ?= .git

BINPERM  = 755
DIRPERM  = 755
FILEPERM = 644


###############################################
# Here are where Inform's pieces are installed.
BINDIR         = $(PREFIX)/bin
SHAREDIR       = $(PREFIX)/share/$(BINNAME)


##########################
# For the Standard Library
STDSRC         = std
STD            = $(SHAREDIR)/$(STDSRC)
STDLIB         = $(STD)/lib
STDLIB_LINKS   = English.h Grammar.h Parser.h Verblib.h VerbLib.h
STDINC         = $(STD)/include
STDDEMO        = $(STD)/demos
STDTUTOR       = $(STD)/tutor

STDLIB_SRC     = $(STDSRC)
STDINC_SRC     = std-include
STDDEMOS_SRC   = std-demos
STDTUTOR_SRC   = std-tutor


############################
# For the PunyInform Library
PUNYSRC         = punyinform
PUNY            = $(SHAREDIR)/$(PUNYSRC)
PUNYLIB         = $(PUNY)/lib
PUNYINCLUDE     = $(PUNY)/include
PUNYHOWTO       = $(PUNY)/howto
PUNYTESTS       = $(PUNY)/tests
PUNYDOCS        = $(PUNY)/documentation

PUNYDOCS1       = $(PUNYDOCS)/guides
PUNYDOCS2       = $(PUNYDOCS)/screenshots
PUNYDOCS3       = $(PUNYDOCS)/technical

PUNYDOCS_SRC     = $(PUNYSRC)/documentation
PUNYDOCS_DIR1    = $(PUNYDOCS_SRC)/guides
PUNYDOCS_DIR2    = $(PUNYDOCS_SRC)/screenshots
PUNYDOCS_DIR3    = $(PUNYDOCS_SRC)/technical

PUNYLIB_SRC      = $(PUNYSRC)/lib
PUNYINC_SRC      = $(PUNYSRC)/include
PUNYHOWTO_SRC    = $(PUNYSRC)/howto
PUNYTESTS_SRC    = $(PUNYSRC)/tests

PUNYINFORM       = punyinform
PUNYINFORMSH     = punyinform.sh
PUNYMISC_FILES   = LICENSE README.md releasenotes.txt


#######################################################
# For the Compiler
# The default include path is for the Standard Library.
# The PunyInform Library sets its own path.
INCLUDEPATH   = ",$(STDLIB),$(STDINC)"

DEFINES= -DUNIX -DDefault_Language=\"$(LANGUAGE)\" \
	-DInclude_Directory=\"$(INCLUDEPATH)\" \
	-DTemporary_Directory=\"$(TEMPDIR)\"

COMPSRC = src
SOURCES = $(wildcard ${COMPSRC}/*.c)
OBJECTS = $(patsubst %.c,%.o,${SOURCES})

MANPAGE       = $(NAME).1
MANDIR        = $(PREFIX)/man/man1
TEMPDIR       = /tmp

# Check to see if this machine's filesystem is case-sensitive or not.
# If it is, we need to set up some symbolic links to deal with this.
ifneq ($(shell touch $(STDLIB)/ctesta $(STDLIB)/ctestA 2>/dev/null && ls $(STDLIB)/ctest? | wc -l | sed 's/^ *//g'), 2)
CASETEST = yes
endif

.PHONY: all clean distclean stdlib punylib

all:	$(BINNAME) stddemos stdtutor punyhowto punytests

#############################################################
# Submodules must be downloaded before anything else happens.
submodule: submodules
submodules:
ifneq ($(and $(wildcard $(GIT_DIR)),$(shell which $(GIT))),)
	rm -rf $(COMPSRC) $(STDSRC) $(PUNYSRC)
	@echo "** Downloading compiler and library source code..."
	@$(GIT) submodule init
	@$(GIT) submodule update
	@$(GIT) submodule update
else
	@echo "Not in a git repository or git command missing."
	@echo "Just try \"make\" now."
	@echo "If you're still having problems, you may have downloaded an archive from"
	@echo "Gitlab instead of doing a clone.  Read README.md."
endif


########################################################
# Rules for building and installing the Inform6 Compiler
%.o: %.c
	$(CC) $(DEFINES) $(OPTS) -o $@ -c $<

$(BINNAME): $(OBJECTS)
	@echo "** Don't download this package directly from Gitlab.  You need to clone it"
	@echo "** from Gitlab or download an archive from the Interactive Fiction Archive."
	@echo "** See also README.md for anything you may have missed."
	@echo "** If the build right away, you may need to do \"make submodules\" first."
	$(CC) -o $(BINNAME) $(LDFLAGS) $(OBJECTS)
	@echo "** Done building compiler."

strip:	$(BINNAME)
	strip $(BINNAME)

install-compiler: $(BINNAME)
	install -d -m $(DIRPERM) $(DESTDIR)$(BINDIR)
	install -c -m $(BINPERM) $(BINNAME) $(DESTDIR)$(BINDIR)

#########################################################
# Rules for building and installing the Standard Library
%.z5: %.inf | $(BINNAME)
	$(PWD)/$(BINNAME) +$(STDSRC) $< $@

stddemos:	$(BINNAME) $(STDDEMOS_Z5)
	@echo "** Done building Standard Library demos."

stdtutor:	$(BINNAME) $(STDTUTOR_Z5)
	@echo "** Done building Standard Library tutorials."

install-stdlib-all: install-stdlib install-stdinc install-stddemos install-stdtutor

install-stdlib:
	install -d -m $(DIRPERM) $(STDLIB)
	install -c -m $(FILEPERM) $(wildcard ${STDSRC}/*) $(STDLIB)
	@rm -f $(STDLIB)/ctest?
ifdef CASETEST
	@echo "Adding library symlinks to installation."
	@ cd $(STDLIB);				\
	for file in $(STDLIB_LINKS); do				\
		realfile=`echo $$file | tr '[A-Z]' '[a-z]'`;	\
		echo "  $$file -> $$realfile";			\
		test -r $$file || ln -sf $$realfile $$file;	\
	done
else
	@echo "Not adding library symlinks to installation."
endif
	@echo "** Done installing Standard Library."

install-stdinc:
	install -d -m $(DIRPERM) $(STDINC)
	install -c -m $(FILEPERM) $(wildcard ${STDINC_SRC}/*) $(STDINC)
	@echo "** Done installing Standard Library includes."

install-stddemos: stddemos
	install -d -m $(DIRPERM) $(STDDEMO)
	install -c -m $(FILEPERM) $(wildcard ${STDDEMOS_SRC}/*) $(STDDEMO)
	@echo "** Done installing Standard Library demos."

install-stdtutor: stdtutor
	install -d -m $(DIRPERM) $(STDTUTOR)
	install -c -m $(FILEPERM) $(wildcard ${STDTUTOR_SRC}/*) $(STDTUTOR)
	@echo "** Done installing Standard Library tutorials."


##########################################################
# Rules for building and installing the PunyInform Library
%.z3: %.inf | $(BINNAME)
	$(PWD)/$(BINNAME) +$(PUNYLIB_SRC) $< $@

punyhowto:	$(BINNAME) $(PUNYHOWTO_Z3)
	@echo "** Done building PunyInform Library howtos."

punytests:	$(BINNAME) $(PUNYTESTS_Z3)
	@echo "** Done building PunyInform Library tests."

install-punywrapper:
	install -c -m $(BINPERM) $(PUNYINFORMSH) $(DESTDIR)$(BINDIR)
	sed -i "s,^\s*LIBPATH.*,LIBPATH=$(SHAREDIR),g" $(DESTDIR)$(BINDIR)/$(PUNYINFORMSH)
	cd $(DESTDIR)$(BINDIR) ; ln -sf $(PUNYINFORMSH) $(PUNYINFORM)

install-punylib:
	install -d -m $(DIRPERM) $(PUNYLIB)
	install -c -m $(FILEPERM) $(wildcard ${PUNYLIB_SRC}/*) $(PUNYLIB)
	@echo "** Done installing PunyInform Library."

install-punyhowto:
	install -d -m $(DIRPERM) $(PUNYHOWTO)
	install -c -m $(FILEPERM) $(wildcard ${PUNYHOWTO_SRC}/*) $(PUNYHOWTO)

install-punytests:
	install -d -m $(DIRPERM) $(PUNYTESTS)
	install -c -m $(FILEPERM) $(wildcard ${PUNYTESTS_SRC}/*) $(PUNYTESTS)

install-punydocs:
	install -d -m $(DIRPERM) $(PUNYDOCS)
	install -d -m $(DIRPERM) $(PUNYDOCS1)
	install -d -m $(DIRPERM) $(PUNYDOCS2)
	install -d -m $(DIRPERM) $(PUNYDOCS3)
	install -c -m $(FILEPERM) $(wildcard ${PUNYDOCS_SRC}/*pdf) $(PUNYDOCS)
	install -c -m $(FILEPERM) $(wildcard ${PUNYDOCS_DIR1}/*) $(PUNYDOCS1)
	install -c -m $(FILEPERM) $(wildcard ${PUNYDOCS_DIR2}/*) $(PUNYDOCS2)
	install -c -m $(FILEPERM) $(wildcard ${PUNYDOCS_DIR3}/*) $(PUNYDOCS3)

install-punymisc:
	for file in $(PUNYMISC_FILES); do \
		install -c -m $(FILEPERM) $(PUNYSRC)/$$file $(PUNY); \
	done
	install -c -m $(FILEPERM) $(wildcard ${PUNYSRC}/*.inf) $(PUNY)

install-puny:	install-punylib install-punywrapper install-punymisc install-punyhowto install-punytests install-punydocs


#############################
# Installing everything else

install: install-compiler install-manual install-contrib install-stdlib install-puny

install-manual:
	install -d -m $(DIRPERM) $(MANDIR)
	install -c -m $(FILEPERM) $(MANPAGE) $(MANDIR)

install-contrib:
	install -c -m $(BINPERM) contrib/pblorb.pl $(DESTDIR)$(BINDIR)
	install -c -m $(BINPERM) contrib/scanblorb.pl $(DESTDIR)$(BINDIR)
	cd $(DESTDIR)$(BINDIR) ; ln -sf pblorb.pl pblorb
	cd $(DESTDIR)$(BINDIR) ; ln -sf scanblorb.pl scanblorb

install-strip: strip install

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/$(BINNAME)
	rm -f $(DESTDIR)$(BINDIR)/pblorb*
	rm -f $(DESTDIR)$(BINDIR)/scanblorb*
	rm -f $(DESTDIR)$(BINDIR)/$(PUNYINFORM)*
	rm -f $(MANDIR)/$(MANPAGE)
	rm -rf $(SHAREDIR)

dist: distclean
	mkdir $(DISTNAME)
	@for file in `ls`; do \
		if test $$file != $(DISTNAME); then \
			cp -Rp $$file $(DISTNAME)/$$file; \
		fi; \
	done
	find $(DISTNAME) -type l -exec rm -f {} \;
	rm -rf $(DISTNAME)/${COMPSRC}/.git* $(DISTNAME)/${COMPSRC}/.deps
	rm -rf $(DISTNAME)/${STDLIB_SRC}/.git* $(DISTNAME)/${STDLIB_SRC}/.deps
	rm -rf $(DISTNAME)/${PUNYLIB_SRC}/.git* $(DISTNAME)/${PUNYLIB_SRC}/.deps
	tar chof $(DISTNAME).tar $(DISTNAME)
	gzip -f --best $(DISTNAME).tar
	rm -rf $(DISTNAME)
	@echo
	@echo "$(DISTNAME).tar.gz created"
	@echo

clean:
	rm -f $(BINNAME)
	rm -f ${COMPSRC}/*.o
	rm -f ${STDDEMOS_SRC}/*z5
	rm -f ${STDTUTOR_SRC}/*z5
	rm -f $(STDLIB_SRC)/ctest? $(STDLIB_SRC)/ctest?
	rm -f $(PUNYHOWTO_SRC)/*z?
	rm -f $(PUNYTESTS_SRC)/*z?

gitclean:
ifneq ($(and $(wildcard $(GIT_DIR)),$(shell which $(GIT))),)
	$(GIT) clean -fdx
else
	@echo "Not in a git repository or git command missing."
endif

distclean: clean
	find . -name *core -exec rm -f {} \;
	-rm -rf $(DISTNAME)
	-rm -f $(DISTNAME).tar $(DISTNAME).tar.gz


